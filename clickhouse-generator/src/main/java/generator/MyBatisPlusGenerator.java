package generator;

import java.sql.SQLException;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * @author: pzx
 * @date: 2021/3/26
 * @description: mybatis-plus 自动生成器
 */
public class MyBatisPlusGenerator {

    public static void main(String[] args) throws SQLException {

        //1. 全局配置
        GlobalConfig config = new GlobalConfig();
        // 是否支持AR模式
        config.setActiveRecord(true)
                .setAuthor("JUSTIN G")
                //.setOutputDir("D:\\workspace_mp\\mp03\\src\\main\\java")
                // 生成路径
                .setOutputDir("E:\\MyBatisPlusGenerator\\src\\main\\java")
                // 文件覆盖
                .setFileOverride(true)
                // 主键策略
                // 设置生成的service接口的名字的首字母是否为I
                .setServiceName("%sService")
                // IEmployeeService
                //生成基本的resultMap
                .setBaseResultMap(true)
                //生成基本的SQL片段
                .setBaseColumnList(true)
                //取消二级缓存配置
                .setEnableCache(false)
                //文件覆盖
                .setFileOverride(true)
                //设置日期类型为date， 高版本的mybatits-plus为LocalDateTime不兼容Druid
                .setDateType(DateType.ONLY_DATE)
        ;

        //2. 数据源配置
        DataSourceConfig dsConfig = new DataSourceConfig();
        // 设置数据库类型
        dsConfig.setDbType(DbType.MYSQL)
                .setDriverName("com.mysql.jdbc.Driver")
                .setUrl("jdbc:mysql://rdsuubnuauubnua623.mysql.rds.aliyuncs.com:3306/clickhouse")
                .setUsername("link_sale_user")
                .setPassword("kKh4oPdEXBTLctfGFVsx2Z6YAbieH3v9");

        //3. 策略配置globalConfiguration中
        StrategyConfig stConfig = new StrategyConfig();
        //全局大写命名
        stConfig.setCapitalMode(true)
                // 设置lombok模型
                .setEntityLombokModel(true)
                // 数据库表映射到实体的命名策略
                .setNaming(NamingStrategy.underline_to_camel)
                //.setTablePrefix("tb_")
                // 生成的表
                .setInclude("test_table");

        //4. 包名策略配置
        PackageConfig pkConfig = new PackageConfig();
        pkConfig.setParent("com.jds.clickhouse.model")
                //dao
                .setMapper("mapper.portray")
                //servcie
                .setService("service.portray")
                //controller
                .setController("controller.portray")
                .setEntity("entity.portray")
                //mapper.xml
                .setXml("mapper.portray");
        //5. 整合配置
        AutoGenerator ag = new AutoGenerator();
        ag.setGlobalConfig(config)
                .setDataSource(dsConfig)
                .setStrategy(stConfig)
                .setPackageInfo(pkConfig);
        //6. 执行
        ag.execute();
    }

}