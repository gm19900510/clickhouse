package com.jds.clickhouse.model.entity.mysql;

import java.util.Date;
import java.io.Serializable;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 客户群体与对应员工的所属关系
 * </p>
 *
 * @author JACKSON G
 * @since 2020-12-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AffiliationRelationMysql extends Model<AffiliationRelationMysql> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String oneId;
    private String companyWxid;
    private String customerWxid;
    private String showHeadUrl;
    private Long type;
    private Integer isChatRoom;
    private String labels;
    private String conRemark;
    private String employeeName;
    private String employeeGroup;
    private String employeeId;
    private Integer relationStatus;
    private Integer lossStatus;
    private Date addTime;
    private Date createTime;
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
