package com.jds.clickhouse.model.mapper.clickhouse;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jds.clickhouse.model.entity.mysql.AffiliationRelationClickhouse;
import com.jds.clickhouse.model.entity.mysql.AffiliationRelationMysql;


/**
 * <p>
 * 客户群体与对应员工的所属关系 Mapper 接口
 * </p>
 *
 * @author zhixiang.peng
 * @since 2020-12-01
 */
@DS("slave")
public interface AffiliationRelationClickhouseMapper extends BaseMapper<AffiliationRelationClickhouse> {


}
