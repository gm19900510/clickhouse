package com.jds.clickhouse.web.ResultJson;


import com.google.common.base.MoreObjects;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author: zhixiang.peng
 * @date: 2020/10/27
 * @description: 请求统一返回的json格式
 */
@Getter
@Setter
@NoArgsConstructor
public class ResultJson<T> {

        public static final int SUCC_CODE = 0;

        public static final String SUCC_MSG = "成功";

        public static final int FAIL_CODE = -1;

        public static final String FAIL_MSG = "失败";

        private int code;

        private String message;

        private T result;

        public ResultJson(T result) {
            this.message = SUCC_MSG;
            this.result = result;
        }

        public ResultJson(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public static <T> ResultJson<T> success(T vo) {
            ResultJson<T> result = new ResultJson<>();
            result.code = 0;
            result.message=SUCC_MSG;
            result.result = vo;
            return result;
        }

        public static ResultJson<Object> success() {
            ResultJson<Object> result = new ResultJson<>();
            result.code = 0;
            result.message=SUCC_MSG;
            return result;
        }
        public static ResultJson<Object> success(String msg,Object t) {
            ResultJson<Object> result = new ResultJson<>();
            result.setCode(0);
            result.setMessage(msg);
            result.setResult(t);
            return result;
        }

        public static <T> ResultJson<T> fail(T t) {
            ResultJson<T> result = new ResultJson<>();
            result.setCode(FAIL_CODE);
            result.setMessage(FAIL_MSG);
            result.setResult(t);
            return result;
        }

        public static ResultJson<Object> fail() {
            ResultJson<Object> result = new ResultJson<>();
            result.setCode(FAIL_CODE);
            result.setMessage(FAIL_MSG);
            return result;
        }

        public static ResultJson<Object> failMsg(String msg) {
            ResultJson<Object> result = new ResultJson<>();
            result.setCode(FAIL_CODE);
            result.setMessage(msg);
            result.setResult(null);
            return result;
        }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("code", this.code)
                .add("message", this.message)
                .add("result", this.result)
                .omitNullValues().toString();
    }

    }
