package com.jds.clickhouse.web;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author: zhixiang.peng
 * @date: 2021/3/26
 * @description:
 */
@ComponentScan({"com.jds.clickhouse"})
@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
@MapperScan({"com.jds.clickhouse.model.mapper"})
public class ClickhouseWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClickhouseWebApplication.class, args);
    }

}
